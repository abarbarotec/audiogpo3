﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetected : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip collisionClip;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //audioSource.PlayOneShot(collisionClip);

        AudioSource.PlayClipAtPoint(collisionClip, this.transform.position);

        Destroy(gameObject);

        // codigo modificado en otro branch

        // modificar branch de jira
    }
}
